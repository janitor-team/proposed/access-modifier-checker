Source: access-modifier-checker
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: James Page <jamespage@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 junit4,
 libannotation-indexer-java (>= 1.3),
 libasm-java (>= 9.0),
 libfindbugs-annotations-java,
 libmaven-plugin-tools-java (>= 2.8),
 libmaven-scm-java,
 libmaven3-core-java,
 libmetainf-services-java,
 maven-debian-helper (>= 2.0)
Standards-Version: 4.6.0.1
Vcs-Git: https://salsa.debian.org/java-team/access-modifier-checker.git
Vcs-Browser: https://salsa.debian.org/java-team/access-modifier-checker
Homepage: https://github.com/kohsuke/access-modifier

Package: libaccess-modifier-checker-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: Maven plugin for custom access modifier checking
 This maven plugin allows applications to define custom
 access modifiers programmatically, to be enforced at
 compile time in the opt-in basis. Obviously, there's no
 runtime check either --- this is strictly a voluntary
 annotation.
 .
 This mechanism is useful for actually making sure that
 deprecated features are not used (without actually removing
 such declarations, which would break binary compatibility.)
