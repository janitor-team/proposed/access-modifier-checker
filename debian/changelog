access-modifier-checker (1.27-2) unstable; urgency=medium

  * Team upload.
  * Ignore git-changelist-maven-extension

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 12 Oct 2022 08:39:11 +0200

access-modifier-checker (1.27-1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 30 Apr 2022 15:03:25 +0200

access-modifier-checker (1.25-1) unstable; urgency=medium

  * Team upload.
  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 02 Oct 2021 21:25:03 +0200

access-modifier-checker (1.24-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Standards-Version updated to 4.6.0.1

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 17 Sep 2021 09:54:01 +0200

access-modifier-checker (1.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Depend on libasm-java (>= 9.0)
    - Depend on junit4 instead of junit
    - New dependency on libfindbugs-annotations-java
    - Set the source/target level to Java 8
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 04 Jan 2021 17:17:43 +0100

access-modifier-checker (1.16-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Build the new access-modifier-suppressions module
  * Removed the -java-doc package
  * Standards-Version updated to 4.3.0

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 31 Jan 2019 00:03:34 +0100

access-modifier-checker (1.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Removed the unused build dependency on libmaven-install-plugin-java
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 04 Jun 2018 21:46:25 +0200

access-modifier-checker (1.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Depend on libmaven3-core-java instead of libmaven2-core-java
  * Set the source encoding to UTF-8
  * Standards-Version updated to 4.0.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 31 Jul 2017 10:20:31 +0200

access-modifier-checker (1.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Removed 01-upgrade-asm.patch (fixed upstream)
  * Depend on libasm-java (>= 5.0) instead of libasm4-java
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL
  * debian/watch: No longer use githubredir.debian.net

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 27 Sep 2016 23:39:58 +0200

access-modifier-checker (1.4-2) unstable; urgency=medium

  * Team upload.
  * Depend on libasm4-java instead of libasm3-java
  * debian/control:
    - Standards-Version updated to 3.9.5 (no changes)
    - Use canonical URLs for the Vcs-* fields
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 15 Sep 2014 12:48:15 +0200

access-modifier-checker (1.4-1) unstable; urgency=low

  * New upstream release.
  * d/control: Dropped DM-Upload-Allowed, bumped Standards-Version 3.9.4
    - no changes.
  * d/control: Update my email address.

 -- James Page <jamespage@debian.org>  Mon, 12 Aug 2013 16:36:15 +0100

access-modifier-checker (1.0-4) unstable; urgency=low

  [ James Page ]
  * Use new version of annotation-indexer:
    - d/maven.rules: Map to new org.jenkins-ci groupID.
    - d/control: Added version to BDI to ensure compatibility.
  * Bumped Standards-Version to 3.9.3:
    - d/copyright: Use new released version of DEP-5, updated field
      names.

  [ tony mancill ]
  * Set DMUA flag.

 -- James Page <james.page@ubuntu.com>  Mon, 30 Apr 2012 15:01:54 +0100

access-modifier-checker (1.0-3) unstable; urgency=low

  * Team upload.
  * Fix maven rule for asm-debug-all. (Closes: #642679)

 -- Torsten Werner <twerner@debian.org>  Sat, 24 Sep 2011 21:51:53 +0200

access-modifier-checker (1.0-2) unstable; urgency=low

  * Team upload.
  * Add Build-Depends on libmaven-plugin-tools-java (>= 2.8) (Closes: #639828).

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 30 Aug 2011 13:31:54 -0430

access-modifier-checker (1.0-1) unstable; urgency=low

  * Initial Debian release (Closes: #629909)

 -- James Page <james.page@ubuntu.com>  Mon, 01 Aug 2011 10:06:23 +0100

access-modifier-checker (1.0-0ubuntu1) oneiric; urgency=low

  * Initial release

 -- James Page <james.page@canonical.com>  Thu, 09 Jun 2011 14:34:52 +0100
